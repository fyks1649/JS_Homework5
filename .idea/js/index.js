'use strict'

let object = {a: 1, b: 2, c: {
        c: 6
    }}

function deepCopy(object) {
    if (typeof object != "object") {
        return object;
    }

    let copy = {}
    for (let key in object) {
        if (typeof object[key] == "object") {
            copy[key] = deepCopy(object[key]);
        } else {
            copy[key] = object[key];
        }
    }
    return copy;
};
console.log(object)
let deepClone = deepCopy(object)
console.log(deepClone)